const request = require("request");
const search = require("youtube-search");
//const ytdl = require("youtube-dl");
const ytdl = require("ytdl-core");

const opts = {
  part: "snippet",
  maxResults: 10,
  key: xavio.config.googleApiKey
};

let queues = xavio.queues;

function playlistItems(link) {
  return new Promise((resolve, reject) => {
    const id = /\?(?:v|list)=(\w*)/.exec(link)[1];
    request(`https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2C+id&playlistId=${id}&key=${xavio.config.googleApiKey}&maxResults=25`, (error, response, body) => {
      if(error || response.statusCode !== 200){
        reject(error);
      } else {
        const data = JSON.parse(body);
        if(data.error){
          reject(`Playlist with id *"${id}"* does not exist!`);
        } else {
          const ids = data.items.map(e => e.snippet.resourceId.videoId);
          resolve(ids);
        }
      }
    });
  });
}

exports.playlistItems = playlistItems;

exports.videoId = (link) => {
  return new Promise((resolve, reject) => {
    const id = /\?(?:v|list)=(\w*)/.exec(link)[1];
    id ? resolve(id) : reject("No video ID found!");
  });
};

function identifyType(link) {
  const info = /^(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?.*?(?:v|list)=(.*?)(?:&|$)|^(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?(?:(?!=).)*\/(.*)$/.exec(link);
  const sc = /^https?:\/\/(soundcloud\.com|snd\.sc)\/(.*)$/;
  if(info){
    if(info[1].startsWith("PL") && info[1].length > 11){
      return "playlist";
    } else if(info[1].length == 11){
      return "video";
    } else {
      return "unknown";
    }
  } else if(sc.exec(link) && sc.exec(link)[2]){
    return "soundcloud";
  } else {
    return "unknown";
  }
}

exports.identifyType = identifyType;

exports.getQueue = (guild) =>{
    if (!guild) return;
    if (typeof guild == "object"){
        guild = guild.id;
    }
    if (queues[guild]){
        return queues[guild];
    }else{
        queues[guild] = [];
    }
    return queues[guild];
};

function play(msg, queue, song) {
    if(!msg || !queue) return;
    if(song){
        search(song, opts, (err, res)=>{
            if(err){
              console.log(err.stack); return msg.channel.createMessage("Video was not found! Please try again with the video url");
            }
            song = (song.startsWith("https://" || "http://")) ? song : res[0].link;
            try{
                let stream = song;
            }catch(e){
                console.log(e);
                msg.channel.createMessage(msg.channel.id, "Uh-oh! I ran into an error!");
            }
            let canPlay;
            if(queue.length == 0){
                canPlay = true;
            }else{
                canPlay = false;
            }
            function p(n){
              var pos = (queue.length == 1) ? "Up Next!" : queue.length.toString();
              msg.channel.createMessage("Queued **" + queue[queue.length - 1].title + "**, Position in queue: "+pos).then(m=>{setTimeout(()=>{xavio.deleteMessage(m.channel.id, m.id);}, 8000);});
              if(canPlay){
                if(!n || n < 1){
                  setTimeout(()=>{
                      play(msg, queue);
                  }, 3000);
                }
              }
            }

            const type = identifyType(song);
            if(type == "playlist"){
              playlistItems(song).then(items => {
                items.forEach((e, i) => {
                  const link = `https://www.youtube.com/watch?v=${e}`;
                  ytdl.getInfo(link, (error, info) => {
                    if(error) return msg.channel.createMessage("Uh-oh! I ran into an error: "+error);
                    queue.push({
                        "title": info.title,
                        "requestor": msg.author.username,
                        "link": link
                    });
                    p(i);
                  });
                });
              }).catch((err) => {
                msg.channel.createMessage("Uh-oh! I ran into an error: "+err);
                xavio.logger.warn(err);
              });
            } else if(type == "video") {
              ytdl.getInfo(song, (error, info) => {
                if(error) return msg.channel.createMessage("Uh-oh! I ran into an error: "+error);
                queue.push({
                    "title": info.title,
                    "requestor": msg.author.username,
                    "link": song
                });
                p();
              });
            } else if(type == "soundcloud") {
              let lk;
              if(/https?:\/\/snd\.sc/.exec(song) && /https?:\/\/snd\.sc/.exec(song)[0]){
                 request(song, (error,response,body) => {
                   lk = response.request.uri.href;
                   f1();
                 });
              } else {
                lk = song;
                f1();
              }
              function f1(){
                request(`https://api.soundcloud.com/resolve.json?url=${lk}&client_id=${xavio.config.soundcloudClientID}`, (error, response, body) => {
                  if(error) return msg.channel.createMessage("Uh-oh! I ran into an error");
                  try {
                    const data = JSON.parse(body);
                    queue.push({
                        "title": data.title,
                        "requestor": msg.author.username,
                        "link": lk
                    });
                    p();
                  } catch(e) {
                    xavio.logger.warn(e);
                    return msg.channel.createMessage("Uh-oh! I ran into an error");
                  }
                });
              }
            }
            // msg.channel.permissionsOf(bot.user.id);
            // MSG.then(m=>{setTimeout(()=>{bot.deleteMessage(m.channel.id, m.id)}, 5000)});
            // msg.channel.guild.members.find(o => o.id === bot.user.id);
            // msg.channel.guild.members.get(bot.user.id)
            // bot.createMessage(bot.guilds.find((e)=>e.name == "[Name]").channels.find((e)=>e.name == "[Channel]").id, "[Content]");
        });
    }else if(queue.length !== 0){
        msg.channel.createMessage(`Now Playing **${queue[0].title}** | by **${queue[0].requestor}**`).then(m=>{setTimeout(()=>{xavio.deleteMessage(m.channel.id, m.id);}, 8000);});
        try{
            var connection = xavio.voiceConnections.get(msg.channel.guild.id);
            if(!connection) return;
        }catch(e){msg.channel.createMessage("Uh-oh! got an error: "+e);}
        try{
            if(connection.playing) return;
            connection.play(ytdl(queue[0].link));
            connection.once("end", ()=>{
                queue.shift();
                if(queue.length !== 0){
                    play(msg, queue);
                    return;
                }else{
                    msg.channel.createMessage("Queue is empty, now leaving current voice channel").then(m=>{setTimeout(()=>{xavio.deleteMessage(m.channel.id, m.id);}, 8000);});
                    xavio.voiceConnections.leave(msg.channel.guild.id);
                    return;
                }
            });
            connection.on("error", (e) => {
              msg.channel.createMessage("Uh-oh! I ran into an error: "+e);
            });
        }catch(e){
            console.log(e);
            msg.channel.createMessage("Uh oh! got an error: "+e.message);
        }
    }else{
        msg.channel.createMessage("Queue is empty!").then(m=>{setTimeout(()=>{xavio.deleteMessage(m.channel.id, m.id);}, 8000);});
    }
}

exports.play = play;
