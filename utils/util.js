const c = require("chalk");
const chalk = new c.constructor({enabled: true});

const ytRegex = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;

exports.jsonDate = () => JSON.parse(JSON.stringify(new Date()));

exports.parsePrefix = function(prefix, content) {
  if (typeof prefix === "string") {
		if(content.startsWith(prefix)){
			return {success: true, parsed: content.substring(prefix.length), prefix: prefix};
		} else {
			return {success: false};
		}
  } else {
		let possible = [];
    let ping = false;
		prefix.forEach(p => {
      if(p == "@mention") { p = xavio.user.mention; }
      if(content.startsWith(p)) { possible.push(p); if (p == xavio.user.mention) ping = true; }
    });
		if(possible.length == 0){
			return {success: false};
    } else {
			const longest = possible.reduce(function (a, b) { return a.length > b.length ? a : b; });
			return {success: true, parsed: content.substring(longest.length), prefix: longest, mention: ping};
    }
	}
};

exports.findUser = (query, members) => {
	if (!query || !members || typeof query != "string") return false;
	let r = members.find(m => !m.username ? false : m.username.toLowerCase() == query.toLowerCase());
	if (!r) r = members.find(m => !m.username ? false : m.username.toLowerCase().indexOf(query.toLowerCase()) == 0);
	if (!r) r = members.find(m => !m.username ? false : m.username.toLowerCase().includes(query.toLowerCase()));
	if (!r) r = members.find(m => !m.nick ? false : m.nick.toLowerCase() === query.toLowerCase());
	if (!r) r = members.find(m => !m.nick ? false : m.nick.toLowerCase().indexOf(query.toLowerCase()) === 0);
	if (!r) r = members.find(m => !m.nick ? false : m.nick.toLowerCase().includes(query.toLowerCase()));
	return r || false;
};

exports.comma = (num) => num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");


exports.custom = () => {
	const args = [...arguments];
	if(args % 2 == 0) return;
	let msg = "";
	args.forEach((e, i) => { if(i % 2 !== 0) { msg += "chalk"; e.split(".").forEach(e => msg += "['"+e+"']"); msg += "('"+args[i - 1]+"')"; if(i + 1 !== args.length) msg += " + ";} });
	return console.log(eval(msg));
};

exports.msFormat = (msec) => {
	var days = Math.floor(msec / 1000 / 60 / 60 / 24);
	msec -= days * 1000 * 60 * 60 * 24;
	var hours = Math.floor(msec / 1000 / 60 / 60);
	msec -= hours * 1000 * 60 * 60;
	var mins = Math.floor(msec / 1000 / 60);
	msec -= mins * 1000 * 60;
		var secs = Math.floor(msec / 1000);
	var timestr = "";
	if(days > 0) {
		timestr += days + " days ";
	}
	if(hours > 0) {
		timestr += hours + " hours ";
	}
	if(mins > 0) {
		timestr += mins + " minutes ";
	}
	if(secs > 0) {
		timestr += secs + " seconds ";
	}
	return timestr.trim();
};

exports.correctUsage = (name, prefix, usage, msg) => {
  if(!name || !prefix || !usage || !msg) return;
  prefix = prefix instanceof Array ? prefix[0] : prefix;
  const example = xavio.commands[name].hasOwnProperty("example") ? !!xavio.commands[name].example ? (xavio.commands[name].example instanceof Array ? "\n\n**Examples: **\n • \`"+prefix+xavio.commands[name].example.join("\`\n• \`"+prefix)+"\`" : "\n\n**Example: ** • \`"+prefix+xavio.commands[name].example+"\`") : "" : "";
  msg.channel.createMessage(`**${msg.author.username.replace(/@/, "\u200b")}**, The correct usage is \`${prefix}${name} ${usage}\`${example}\n\nFor more information on this command, do \`${prefix}describe ${name}\``);
};

exports.youtubeParser = (url) => {
  const match = url.match(ytRegex);
  return (match&&match[7].length==11) ? match[7] : false;
};
