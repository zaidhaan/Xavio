const banish = require("to-zalgo/banish");

module.exports = {
  cooldown: 3,
  usage: "<text>",
  descrpition: "Cleans up the zalgo text",
  process(bot, msg, suffix){
    if(suffix){
      msg.channel.createMessage(banish(suffix));
    } else bot.util.correctUsage("banish", bot.config.prefix, this.usage, msg);
  }
};
