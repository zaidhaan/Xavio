const toUnicode = require("to-unicode");

module.exports = {
  cooldown: 2,
  usage: "<text>",
  description: "Sends the text in smallcaps",
  process(bot, msg, suffix){
    if(suffix){
      msg.channel.createMessage(toUnicode(suffix, "smallCaps"));
    }else bot.util.correctUsage("smallcaps", bot.config.prefix, this.usage, msg);
  }
};
