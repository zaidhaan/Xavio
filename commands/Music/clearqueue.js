module.exports = {
  cooldown: 2,
  description: "Picks a random choice for you",
  aliases: "cleanqueue",
  process(bot, msg, suffix){
    if(!msg.member.permission.has("manageServer") && msg.author.id !== bot.config.ownerId){
        msg.channel.createMessage("You need **Manage Server** permission to clear the queue");
        return;
    }
    if(msg.member.permission.has("manageServer") || msg.author.id == bot.config.ownerId){
        var queue = bot.plugins.music.getQueue(msg.channel.guild.id);
        if(queue.length == 0){
            msg.channel.createMessage("Queue is aleready empty!");
        }else{
            for(var i = 0; i == queue.length - 1; i--){
                queue.splice(i, 1);
            }
            msg.channel.createMessage(msg.channel.id, "Cleared the queue!");
        }
    }
  }
};
