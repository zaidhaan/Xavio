module.exports = {
  cooldown: 5,
  description: "Lists the queue of the current voice channel",
  process(bot, msg, suffix){
    const getQueue = (g) => bot.plugins.music.getQueue(g);
    var queue = getQueue(msg.channel.guild.id);
    if(queue.length == 0){
      bot.createMessage("The queue is empty!");
      return;
    }
    var toSend = [];
    var channelName = bot.getChannel(bot.voiceConnections.get(msg.channel.guild.id).channelID).name;
    toSend.push("**Queue for #"+channelName+" on __"+msg.channel.guild.name+"__:**\n----------------------------------------------------");
    for(var i = 0; i < queue.length; i++){
      toSend.push(`${(i + 1)}. **${queue[i].title}** | by **${queue[i].requestor}**`);
    }
    bot.createMessage(msg.channel.id, toSend.join("\n"));
  }
};
