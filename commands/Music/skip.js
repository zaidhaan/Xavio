module.exports = {
  cooldown: 5,
  description: "Skips the current playing song",
  process(bot, msg, suffix){
    const voice = bot.voiceConnections.get(msg.channel.guild.id);
    const getQueue = (g) => bot.plugins.music.getQueue(x);
    if(!voice || !voice.playing){
      if(!voice.playing && getQueue(msg.channel.guild.id).length >= 1){
        getQueue(msg.channel.guild.id).shift();
        return play(msg, getQueue(msg.channel.guild.id));
      }
      bot.createMessage(msg.channel.id, "The bot is not playing any music!");
      return;
    }
    voice.stopPlaying();
  }
};
