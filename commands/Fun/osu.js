const request = require("request");
const osuapi = require("osu-api");

module.exports = {
  cooldown: 5,
  usage: "sig [username] [hex] | best [username] | user [username] | recent [username]",
  example: ["osu user Cookiezi", "osu best Rafis", "osu recent Doomsday", "osu sig Angelsim"],
  description: "Sends osu information based on the usage",
  process(bot, msg, suffix){
    if(suffix){
      const config = bot.config;
      if(suffix.split(" ")[0] == "sig"){
          let color = "ff66aa";
          let username = msg.author.username;
          suffix = suffix.trim().split(" ");
          suffix.shift();
          if(suffix && suffix.length >= 1){
              if(/(.*) #?[A-Fa-f0-9]{6}$/.test(suffix.join(" "))){
                  username = suffix.join("%20").substring(0, suffix.join("%20").lastIndexOf("%20"));
                  if(suffix[suffix.length - 1].length == 6){
                      color = suffix[suffix.length - 1];
                  }else if(suffix[suffix.length - 1].length == 7){
                      color = suffix[suffix.length - 1].substring(1);
                  }
              }else if(/#?[A-Fa-f0-9]{6}$/.test(suffix.join(" "))){
                  username = msg.author.username;
                  if(suffix[0].length == 6){ color = suffix[0];
                  }else if(suffix[0].length == 7){ color = suffix[0].substring(1); }
              }else{ username = suffix.join("%20"); }
          }
          request({url: "https://lemmmy.pw/osusig/sig.php?colour=hex" + color + "&uname=" + username + "&pp=2&flagshadow&xpbar&xpbarhex&darktriangles", encoding: null,}, function(err, response, body){
              if(err) { bot.createMessage(msg.channel.id, "Uh-oh! I ran into an error: "+err); return;}
              if(response.statusCode != 200){ bot.createMessage(msg.channel.id, "Got status code "+response.statusCode); return;}
              bot.createMessage(msg.channel.id, "Here's your osu signature for **"+username+"**!",{file: body, name: "sig.png"});
          });
      }else if(suffix.split(" ")[0] == "user"){
          let username = (suffix.split(" ").length < 2) ? msg.author.username : suffix.substring(5);
          let osu = new osuapi.Api(config.osuApiKey);
          osu.getUser(username, (err, data)=>{
              if(err) { bot.createMessage(msg.channel.id, "Uh-oh! I ran into an error: "+err); return; }
              if(!data){ bot.createMessage(msg.channel.id, "I couldn't find any user called \""+username+"\""); return; }
              let toSend = [];
              if(data.playcount === null){ bot.createMessage(msg.channel.id, "This user has on data"); return; }
              toSend.push("Osu stats for: **"+data.username+"**:");
              toSend.push("━━━━━━━━━━━━━━━━━━━");
              toSend.push("**PP**: " + data.pp_raw.split(".")[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | \u0020\u0020**Rank**: #" + data.pp_rank.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | \u0020\u0020**Country Rank**: #" + data.pp_country_rank.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " |\u0020\u0020 **Accuracy**: " + data.accuracy.substring(0, data.accuracy.split(".")[0].length + 3) + "%");
              toSend.push("**Play Count**: "+data.playcount.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | \u0020\u0020**Ranked Score**: " + data.ranked_score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | \u0020\u0020**Total Score**: " + data.total_score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | **Level**: " + data.level.substring(0, data.level.split(".")[0].length + 3));
              toSend.push("**300**: " + data.count300.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | \u0020\u0020**100**: " + data.count100.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | \u0020\u0020**50**: " + data.count50.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " | **SS**: " + data.count_rank_ss + " | \u0020\u0020**S**: " + data.count_rank_s + " | \u0020\u0020**A**: " + data.count_rank_a.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
              bot.createMessage(msg.channel.id, toSend.join("\n"));
          });
      }else if(suffix.split(" ")[0] === "best"){
          let username = (suffix.split(" ").length < 2) ? msg.author.username : suffix.substring(5);
          let osu = new osuapi.Api(config.osuApiKey);
          osu.getUserBest(username, (err, data)=>{
              if(err){ bot.createMessage(msg.channel.id, "Uh-oh! I ran into an error:"+err); return; }
              if(!data || !data[0] || !data[1] || !data[2 || !data[4]]){ bot.createMessage(msg.channel.id, "Could not find user\""+username+"\"!"); return; }
              let toSend = [];
              toSend.push("**Top 5 osu scores for **__**"+username+"**__:");
              toSend.push("━━━━━━━━━━━━━━━━━━━");
              osu.getBeatmap(data[0].beatmap_id, (err, map1)=>{
                  toSend.push("\n**1.** *"+map1.title+"* *(☆"+map1.difficultyrating.substring(0, map1.difficultyrating.split(".")[0].length + 3) + ")*: \u0020\u0020**PP:** " + Math.round(data[0].pp.split(".")[0]) + " **| \u0020\u0020Score:** " + data[0].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **|\u0020\u0020 Max Combo:** " + data[0].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **|\u0020\u0020 Misses:** " + data[0].countmiss + " **| \u0020\u0020Date:** " + data[0].date);
                  osu.getBeatmap(data[1].beatmap_id, function(err, map2) {
                      toSend.push("\n**2.** *" + map2.title + "* *(☆" + map2.difficultyrating.substring(0, map2.difficultyrating.split(".")[0].length + 3) + ")*: \u0020\u0020**PP:** " + Math.round(data[1].pp.split(".")[0]) + " **| \u0020\u0020Score:** " + data[1].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Max Combo:** " + data[1].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| \u0020\u0020Misses:** " + data[1].countmiss + " **| \u0020\u0020Date:** " + data[1].date);
                      osu.getBeatmap(data[2].beatmap_id, function(err, map3) {
                          toSend.push("\n**3.** *" + map3.title + "* *(☆" + map3.difficultyrating.substring(0, map3.difficultyrating.split(".")[0].length + 3) + ")*: **PP:** " + Math.round(data[2].pp.split(".")[0]) + " **| \u0020\u0020Score:** " + data[2].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **|\u0020\u0020 Max Combo:** " + data[2].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Misses:** " + data[2].countmiss + " **|\u0020\u0020 Date:** " + data[2].date);
                          osu.getBeatmap(data[3].beatmap_id, function(err, map4) {
                              toSend.push("\n**4.** *" + map4.title + "* *(☆" + map4.difficultyrating.substring(0, map4.difficultyrating.split(".")[0].length + 3) + ")*:\u0020\u0020 **PP:** " + Math.round(data[3].pp.split(".")[0]) + " **| \u0020\u0020Score:** " + data[3].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| \u0020\u0020Max Combo:** " + data[3].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| \u0020\u0020Misses:** " + data[3].countmiss + " **| \u0020\u0020Date:** " + data[3].date);
                              osu.getBeatmap(data[4].beatmap_id, function(err, map5) {
                                  toSend.push("\n**5.** *" + map5.title + "* *(☆" + map5.difficultyrating.substring(0, map5.difficultyrating.split(".")[0].length + 3) + ")*: \u0020\u0020**PP:** " + Math.round(data[4].pp.split(".")[0]) + " **| Score:** " + data[4].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| \u0020\u0020Max Combo:** " + data[4].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| \u0020\u0020Misses:** " + data[4].countmiss + " **| \u0020\u0020Date:** " + data[4].date);
                                  bot.createMessage(msg.channel.id, toSend.join("\n"));
                              });
                          });
                      });
                  });
              });
          });
      }else if(suffix.split(" ")[0] === "recent"){
          let username = (suffix.split(" ").length < 2) ?  msg.author.username : suffix.substring(7);
          let osu = new osuapi.Api(config.osuApiKey);
          osu.getUserRecent(username, function(err, data) {
    if (err) { bot.createMessage(msg.channel.id, "⚠ Error: " + err); return; }
    if (!data || !data[0]) { bot.createMessage(msg.channel.id, "⚠ User \"" + username + "\" not found or no recent plays"); return; }
    let toSend = [];
    toSend.push("5 most recent plays for: **" + username + "**:");
    toSend.push("━━━━━━━━━━━━━━━━━━━");
    osu.getBeatmap(data[0].beatmap_id, function(err, map1) {
      if (!map1 || !map1.title) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
      toSend.push("**1.** *" + map1.title + "* *(☆" + map1.difficultyrating.substring(0, map1.difficultyrating.split(".")[0].length + 3) + ")*: **Score:** " + data[0].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Max Combo:** " + data[0].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Misses:** " + data[0].countmiss);
      if (!data[1]) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
      osu.getBeatmap(data[1].beatmap_id, function(err, map2) {
        if (!map2 || !map2.title) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
        toSend.push("**2.** *" + map2.title + "* *(☆" + map2.difficultyrating.substring(0, map2.difficultyrating.split(".")[0].length + 3) + ")*: **Score:** " + data[1].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Max Combo:** " + data[1].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Misses:** " + data[1].countmiss);
        if (!data[2]) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
        osu.getBeatmap(data[2].beatmap_id, function(err, map3) {
          if (!map3 || !map3.title) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
          toSend.push("**3.** *" + map3.title + "* *(☆" + map3.difficultyrating.substring(0, map3.difficultyrating.split(".")[0].length + 3) + ")*: **Score:** " + data[2].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Max Combo:** " + data[2].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Misses:** " + data[2].countmiss);
          if (!data[3]) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
          osu.getBeatmap(data[3].beatmap_id, function(err, map4) {
            if (!map4 || !map4.title) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
            toSend.push("**4.** *" + map4.title + "* *(☆" + map4.difficultyrating.substring(0, map4.difficultyrating.split(".")[0].length + 3) + ")*: **Score:** " + data[3].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Max Combo:** " + data[3].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Misses:** " + data[3].countmiss);
            if (!data[4]) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
            osu.getBeatmap(data[4].beatmap_id, function(err, map5) {
              if (!map5 || !map5.title) { bot.createMessage(msg.channel.id, toSend.join("\n")); return; }
              toSend.push("**5.** *" + map5.title + "* *(☆" + map5.difficultyrating.substring(0, map5.difficultyrating.split(".")[0].length + 3) + ")*: **Score:** " + data[4].score.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Max Combo:** " + data[4].maxcombo.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " **| Misses:** " + data[4].countmiss);
              bot.createMessage(msg.channel.id, toSend.join("\n"));
                    });
                          });
                      });
                  });
              });
  });
      }else{
          bot.createMessage(msg.channel.id, "Try again with the correct usage: `>sig [username] [hex] | best [username] | user [username] | recent [username]`");
      }
    }else bot.util.correctUsage("osu", bot.config.prefix, this.usage, msg);
  }
};
