const emojiList = {
	"a": "🅰", "b": "🅱", "c": "©", "d": "↩", "e": "📧", "f": "🎏", "g": "⛽",
	"h": "♓", "i": "ℹ", "j": () => Math.random() < .5 ? "🌶" : "🗾", "k": "🎋", "l": "👢", "m": "Ⓜ",
	"n": "♑", "o": () => Math.random() < .5 ? "⭕" : "🔅", "p": "🅿", "q": "📯", "r": "👠", "s": () => Math.random() < .5 ? "💲" : "⚡",
	"t": "🌴", "u": "⛎", "v": () => Math.random() < .5 ? "🖖🏼" : "♈", "w": () => Math.random() < .7 ? "〰" : "📈", "x": () => Math.random() < .5 ? "❌" : "⚔", "y": "✌",
	"z": "Ⓩ", "1": "1⃣", "2": "2⃣", "3": "3⃣", "4": "4⃣", "5": "5⃣",
	"6": "6⃣", "7": "7⃣", "8": "8⃣", "9": "9⃣", "0": "0⃣", "$": "💲", "!": "❗", "?": "❓", " ": "　"
};

module.exports = {
  cooldown: 2,
  usage: "<text>",
	example: "emojify Fabulous",
  description: "Converts the given text into emojis!",
  process(bot, msg, suffix){
    if(suffix){
      bot.createMessage(msg.channel.id, suffix.replace(/./g, (c) => {
				if(emojiList.hasOwnProperty(c.toLowerCase())){
					return typeof emojiList[c.toLowerCase()] === "function" ? emojiList[c.toLowerCase()]() : emojiList[c.toLowerCase()];
				}
				return c;
			}));
    }else bot.util.correctUsage("emojify", bot.config.prefix, this.usage, msg);
  }
};
