const request = require("request");

module.exports = {
  cooldown: 4,
  usage: "<subreddit> <count> [-i]",
  description: "Sends a message with the first 10 or given number of posts from a subreddit",
  process(bot, msg, suffix){
    if(!suffix) return bot.util.correctUsage("reddit", bot.config.prefix, this.usage, msg);
    let path;
    let count = 5;
    let img = false;
    if(suffix.substring(suffix.length - 2) == "-i") { img = true; suffix = suffix.substring(0, suffix.length - 2).trim(); }
    if(suffix){
      if(suffix.indexOf("/r/") == 0 && suffix.length > 3){
        suffix = suffix.slice(3);
      }
      if(suffix.indexOf("r/") == 0 && suffix.length > 2){
        suffix = suffix.slice(2);
      }
      if(suffix.indexOf(" ") > -1){
        var sub = suffix.substring(0, suffix.indexOf(" "));
        count = suffix.substring(suffix.indexOf(" ") + 1);
        if(count.indexOf(" ") > -1){
          count = count.substring(0, count.indexOf(" "));
        }
        path = "/r/" + sub;
      }else{
        sub = suffix.split(" ")[0] || "all";
        count = 5;
      }
      if(!sub || !count || isNaN(count)){
        sub = suffix;
        count = 5;
      }
      if(count < 1 || count > 50){
        count = 5;
      }
      path = "/r/" + sub;
      request(`https://www.reddit.com${path}.json`, (error, response, body) => {
        body = JSON.parse(body);
        if(body.data.children && body.data.children.length > 1){
          let info = "";
          for(let i = 0; i < count; i++){
            let post = body.data.children[i].data;
            if(body.data.children[i].data.over_18 && body.data.children[i].data.over_18 == true && msg.channel.name && msg.channel.name.indexOf("nsfw") == -1){
              break;
            }else if((body.data.children[i].data.over_18 && body.data.children[i].data.over_18 == true && msg.channel.name && msg.channel.name.indexOf("nsfw") > -1) || ( body.data.children[i].data.over_18 && body.data.children[i].data.over_18 == true && !msg.channel.guild)){
              info += `**(NSFW)** **${post.title}**\nhttps://www.reddit.com${post.permalink}${img && post.url ? "\n"+post.url : ""}\n\n`;
            }else{
              info += `**${post.title}**\n<https://www.reddit.com${post.permalink}>${img && post.url ? "\n"+post.url : ""}\n\n`;
            }
          }
          if(info !== undefined || info !== null || info == "") bot.createMessage(msg.channel.id, info);
          else bot.createMessage(msg.channel.id,  `No posts found in **${sub}**`);
        }else bot.createMessage(msg.channel.id, `No results for **${sub}**`);
      });
    }
  }
};
