const answers = ["It is certain!", "It is decidedly so", "Without a doubt!", "Yes definately!", "You may rely on it!", "As I see it, yes!", "Most likely", "Outlook good", "Yes!", "Signs point to yes", "Reply hazy try again", "Ask again later", "Better not tell you now", "Cannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good", "Very doubtful"];

module.exports = {
  cooldown: 2,
  usage: "<question>",
  description: "Answers your questions",
  example: "8ball Is Vex a meme?",
  process(bot, msg, suffix) {
    if (suffix) {
      const answer = answers[~~(Math.random() * answers.length)];
      msg.channel.createMessage({
        embed: {
          color: xavio.colors.DEFAULT,
          fields: [{
              name: ":question: Question:",
              value: suffix
            },
            {
              name: ":8ball: Answer:",
              value: answer
            }
          ]
        }
      });
    } else bot.util.correctUsage("8ball", bot.config.prefix, this.usage, msg);
  }
};
