const request = require("request");

module.exports = {
  cooldown: 3,
  usage: "<poll id/link>",
  description: "Sends info on the given poll",
  process(bot, msg, suffix){
    if(suffix){
      if(parseInt(suffix)){
        request("https://strawpoll.me/api/v2/polls/"+suffix , (error, response, body) => {
          if(!error && response.statusCode == 200){
            let data = JSON.parse(body);
            let ops = data.options;
            let votes = data.votes;
            let snd = "";
            for (let i = 0; i <= ops.length-1; i++) {
              snd += "**"+ops[i]+"** `"+votes[i]+"`\n";
            }
            bot.createMessage(msg.channel.id, "**Title**: "+data.title+"\n**ID**: "+data.id+"\n\n__**Votes**__:\n"+snd);
          }else{
            console.log(cWarn("Got an error: "+error+" Satus code "+response.statusCode));
            bot.createMessage(msg.channel.id, "Sorry! An error occured!");
          }
        });
      }else{
        if(suffix.match(/([0-9])\d+/)){
          request("https://strawpoll.me/api/v2/polls/"+suffix.match(/([0-9])\d+/)[0] , (error, response, body) => {
            let data = JSON.parse(body);
            let ops = data.options;
            let votes = data.votes;
            let snd = "";
            for (let i = 0; i <= ops.length-1; i++) {
              snd += "**"+ops[i]+"** `"+votes[i]+"`\n";
            }
            bot.createMessage(msg.channel.id, "**Title**: "+data.title+"\n**ID**: "+data.id+"\n\n__**Votes**__:\n"+snd);
          });
        }
      }
    }else bot.util.correctUsage("imgur", bot.config.prefix, this.usage, msg);
  }
};
