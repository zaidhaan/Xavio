const {Anime} = require("malapi");

module.exports = {
  usage: "<anime-name>",
  cooldown: 3,
  example: "anime Soushin Shoujo Matoi",
  process(bot, msg, suffix) {
    if (suffix) {
      Anime.fromName(suffix).then(anime => {
        const title = anime.title;
        const episodes = anime.episodes;
        const score = anime.statistics.score.value;
        const type = anime.type;
        const status = anime.status;
        const synopsis = anime.synopsis.length > 400 ? anime.synopsis.substring(0,anime.synopsis.length)+"..." : anime.synopsis;
        const link = anime.detailsLink;
        msg.channel.createMessage(`__**${title}**__\n\n**Type:** ${type} **| Episodes:** ${episodes} **| Status:** ${status} **| Score:** ${score}\n${synopsis}\n**<${link}>**`);
      }).catch(e => {
        msg.channel.createMessage(`No results for "${suffix}"`);
        bot.logger.debug(e);
      });
    } else bot.util.correctUsage("anime", bot.config.prefix, this.usage, msg);
  }
};
