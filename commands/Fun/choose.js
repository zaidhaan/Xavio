module.exports = {
  cooldown: 2,
  usage: "<choice1> | <choice2> [ | <choice3> | etc... ]",
  description: "Picks a random choice for you",
  example: "choose Watch Anime | Play Games",
  aliases: ["choice", "pick"],
  process(bot, msg, suffix){
    if(suffix){
      let choices = suffix.split(/ ?\| ?/);
			if(choices.length < 2 && suffix.includes(",")) choices = suffix.split(/ ?, ?/);
			if(choices.length < 2){
        bot.util.correctUsage("choose", bot.config.prefix, this.usage);
      }else{
				const choice = Math.floor(Math.random() * (choices.length));
				bot.createMessage(msg.channel.id, "I choose **"+choices[choice].replace(/@/g, "\u200b") + "**");
			}
    }else{
      bot.util.correctUsage("choose", bot.config.prefix, this.usage, msg);
    }
  }
};
