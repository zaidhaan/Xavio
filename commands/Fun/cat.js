const request = require("request");

module.exports = {
  cooldown: 3,
  description: "Sends a random cat picture",
  process(bot, msg) {
    request("http://random.cat/meow", (error, response, body) => {
      if (error || response.statusCode !== 200) {
        msg.channel.createMessage("Uh-oh! I ran into an error!");
      } else {
        msg.channel.createMessage({
          embed: {
            title: `Here's your cat, ${msg.author.username}!`,
            url: JSON.parse(body).file,
            color: xavio.colors.DEFAULT,
            image: {
              url: JSON.parse(body).file
            }
          }
        });
      }
    });
  }
};
