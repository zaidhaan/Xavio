const request = require("request");

const defaults = [
  ["Donald", "Trump"],
  ["Hillary", "Clinton"],
  ["Vladimir", "Putin"]
];

module.exports = {
  cooldown: 5,
  usage: "[firstName] [lastName]",
  example: ["joke Emilia", "joke Yuuki Asuna", "joke"],
  description: "Tells a joke about the person you specified",
  process(bot, msg, suffix){
    let name;
    if (suffix) {
      name = suffix.split(" ");
      if (name.length === 1) {
        name = ["", name];
      }
    }else{
      name = defaults[~~(Math.random() * defaults.length)];
    }
    request(`http://api.icndb.com/jokes/random?escape=javascript&firstName=${name[0]}&lastName=${name[1]}`,(error, response, body) => {
      if (!error && response.statusCode == 200) {
        const joke = JSON.parse(body);
        bot.createMessage(msg.channel.id, joke.value.joke);
      }else{
        console.log(error, `\nStatus Code: ${response.statusCode}`)
        msg.channel.createMessage("Uh-oh! I ran into an error!");
      }
    });
  }
};
