const search = require("youtube-search");

module.exports = {
  cooldown: 3,
  usage: "<search-terms>",
  description: "Gives the first youtube search result for the given suffix",
  process(bot, msg, suffix){
    if(!suffix) return bot.util.correctUsage("youtube", bot.config.prefix, this.usage);
    if(bot.config.googleApiKey){
      const opts = {
        part: "snippet",
        maxResults: 10,
        key: bot.config.googleApiKey
      };
      search(suffix, (err, res) => {
        if(err){
          msg.channel.createMessage(`No results for "${suffix}"`);
        }else{
          msg.channel.createMessage(res[0].link);
        }
      });
    }else{
      msg.channel.createMessage("No Google API Key specified by the bot owner!");
    }
  }
};
