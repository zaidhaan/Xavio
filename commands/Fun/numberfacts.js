const request = require("request");

module.exports = {
  cooldown: 5,
  usage: "<number>",
  aliases: ["numfacts", "numfact", "numberfact"],
  description: "Sends information about the given number",
  process(bot, msg, suffix){
    let number = "random";
    if(suffix && /^\d+$/.test(suffix)) number = suffix;
    request("http://numbersapi.com/" + number + "/trivia?json", (error, response, body) => {
      if(!error && response.statusCode == 200){
        const data = JSON.parse(body);
        bot.createMessage(msg.channel.id, data.text);
      } else{
        console.log(error, `\nStatus Code: ${response.statusCode}`)
        msg.channel.createMessage("Uh-oh! I ran into an error!")
      }
    });
  }
};
