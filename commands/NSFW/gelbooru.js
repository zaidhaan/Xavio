const request = require("request");

module.exports = {
  description: "Posts an image from gelbooru with the given tags",
  example: "gelbooru 1girl",
  usage: "<search-terms>",
  process(bot, msg, suffix){
    if(suffix){
      const tag = msg.content.split(" ").pop();
      request(`http://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=100&tags=${tag}`, (error, response, body) => {
        if(error || response.statusCode !== 200) {
          msg.channel.createMessage("Uh-oh! I ran into an error!");
        }else{
          const img = body.match(/file_url=\"(.*?)\"/gm);
          if(img !== null){
            msg.channel.createMessage(`__***${tag}***__\nhttp:`+img[Math.floor(Math.random() * img.length)].replace(new RegExp(/file_url=/g), "").replace(new RegExp(/\"/g), ""));
          }else{
            msg.channel.createMessage(`Couldn't find any images with the tag \`${tag.replace(/\+/g, "  ")}\`"`);
          }
        }
      });
    }else{
      bot.util.correctUsage("gelbooru", bot.config.prefix, this.usage, msg);
    }
  }
};
