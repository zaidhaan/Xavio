const request = require("request");
const xml2js = require("xml2js");


module.exports = {
  cooldown: 2,
  description: "Sends an image from rule34",
  usage: "<search-terms>",
  example: "rule34 titfuck",
  process(bot, msg, suffix){
    if(suffix){
      request(`http://rule34.xxx/index.php?page=dapi&s=post&q=index&limit=50&tags=${suffix.split(" ").join("%20")}`, (error, response, body) => {
        if(error || response.statusCode !== 200){
          msg.channel.createMessage("Uh-oh! I ran into an error!");
        }else{
          xml2js.parseString(body, (err, doc) => {
            if(err) {
              msg.channel.createMessage("Uh-oh! I ran into an error!");
            } else {try{
              let urlList = [];
              if(doc.posts.post !== null){
                for(let i = 0; i < doc.posts.post.length; i++){
                  const imgUrl = doc.posts.post[i].$.file_url;
                  if(imgUrl.endsWith(".gif") || imgUrl.endsWith(".jpg") || imgUrl.endsWith(".png") || imgUrl.endsWith((".jpeg"))){
                    urlList.push(`http:${imgUrl}`);
                  }
                }
              }
              if(urlList.length == 0){
                msg.channel.createMessage(`No results for "${suffix}"`);
              }else{
                bot.createMessage(msg.channel.id, `Found **${urlList.length}/50** posts\n${urlList[Math.floor(Math.random()*urlList.length)]}`);
              }
            }catch(e){
              msg.channel.createMessage("rule34 command isn't working right now. Sorry about that. It will be fixed within thenext 24 hours definately!")
            }}
          });
        }
      });
    }else{
      bot.util.correctUsage("rule34", bot.config.prefix, this.usage, msg);
    }
  }
};
