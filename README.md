# Xavio

Source code of Xavio, being released after the shutdown of Xavio on June 12th, 2018.

If anything seems out of place, please create an issue or make a pull request.

## Running an instance of Xavio
1. Download/Clone this repository
2. Run `npm install` to install all dependencies
3. Add a `config.json` (elaborated below)
4. And finally,  `npm start` to start him up.

Xavio may be hosted on Heroku as well, using the `Procfile` file.

## Configuring `config.json`
Look at `config.sample.json` to get an idea of what it looks like, you may edit this and rename it to `config.json`. It **must** be named `config.json` else the bot will *not* work

- `prefix`: String/Array containing the bot's prefixes. `@mention` means it will respond to mentioning.
- `token`: The bot's token
- `options`: Eris options
- `channels`
   - `bugs`: String containing the ID of the channel where bugs people report will be sent to
   - `suggestions`: String containing the ID of the channel where suggestions will be sent to
- `clientID`: The bot's clientID, used for invites
- `ownerId`: String containing the ID of the person hosting Xavio
- `guildInvite`: Link to the bot's server
- `cleverbot`: Boolean stating whether the bot should use cleverbot
- `imgflipperUser, imgflipperPass, imgurClientId,  giphyApiKey, yandexTranslateKey, dbotsApiKey, carbonKey, googleApiKey, osuApiKey`: Self explanatory

### IMPORTANT NOTE
First of all, I will NOT be adding new features or optimising the code in any way. But however if there is a bug in the code then I may look into it so others willing to selfhost Xavio won't have to face it.

Secondly, since the bot has been shut down, and not been updated for a long time, there may be a few deprecated modules or APIs, meaning some commands may not work. If possible, for the modules, use the exact versions specified in the `package.json`, and for the APIs, well sorry but you're going to have to rewrite that yourself :)

#### RIP Xavio, 4/30/2016 - 6/12/2018.
